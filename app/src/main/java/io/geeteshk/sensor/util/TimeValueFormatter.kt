package io.geeteshk.sensor.util

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter

class TimeValueFormatter : IAxisValueFormatter {

    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        val s = value % 60
        val m = (value / 60) % 60
        val h = (value / (60 * 60)) % 24
        return String.format("%02d:%02d:%02d", h.toInt(), m.toInt(), s.toInt())
    }
}