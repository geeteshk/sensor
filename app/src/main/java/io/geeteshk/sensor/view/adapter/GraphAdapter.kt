package io.geeteshk.sensor.view.adapter

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import io.geeteshk.sensor.R
import io.geeteshk.sensor.util.DataHandler
import io.geeteshk.sensor.util.DataHandler.Companion.dataToMap
import io.geeteshk.sensor.util.TimeValueFormatter
import io.geeteshk.sensor.view.ui.table.TableFragment
import kotlinx.android.synthetic.main.item_graph.view.*
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class GraphAdapter(private var context: Context) : RecyclerView.Adapter<GraphAdapter.ViewHolder>() {

    private var sensorDataMap = TreeMap<Long, List<Float>>()
    private var visibleSensors = intArrayOf(1).toMutableList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_graph, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(visibleSensors[position])
    }

    override fun getItemCount(): Int = visibleSensors.size

    fun updateSensorData(newData: TreeMap<Long, List<Float>>) {
        sensorDataMap = newData
        notifyDataSetChanged()
    }

    fun updateVisibleSensors(newSensors: ArrayList<Int>) {
        visibleSensors = newSensors
        notifyDataSetChanged()
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        private fun setupChart() {
            view.sensorChart.apply {
                setTouchEnabled(true)
                isDragEnabled = true
                setScaleEnabled(true)
                setPinchZoom(false)
                setDrawGridBackground(false)
                setBackgroundColor(Color.TRANSPARENT)
                description.isEnabled = false
                legend.isEnabled = false
                setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
                    override fun onNothingSelected() {
                        onTouchListener.setLastHighlighted(null)
                        highlightValues(null)
                    }

                    override fun onValueSelected(e: Entry?, h: Highlight?) {
                        val x = e!!.x
                        val y = e.y
                        val text = "x = $x\ny = $y"
                        view.sensorValues.text = text
                    }
                })
            }

            val data = LineData()
            data.setValueTextColor(Color.BLACK)
            view.sensorChart.data = data

            val xAxis = view.sensorChart.xAxis
            xAxis.setAvoidFirstLastClipping(true)
            xAxis.setDrawGridLines(false)
            xAxis.valueFormatter = TimeValueFormatter()
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.axisLineColor = Color.BLACK
            xAxis.axisLineWidth = 1.5F
            xAxis.setDrawAxisLine(true)
            xAxis.setDrawGridLines(true)
            xAxis.isGranularityEnabled = true
            xAxis.granularity = 10F
            xAxis.isEnabled = true

            val leftAxis = view.sensorChart.axisLeft
            leftAxis.axisMinimum = 0F
            leftAxis.axisLineWidth = 1.5F
            leftAxis.axisLineColor = Color.BLACK
            leftAxis.isEnabled = true

            val rightAxis = view.sensorChart.axisRight
            rightAxis.isEnabled = false
        }

        fun bind(sensor: Int) {
            view.sensorNumber.text = sensor.toString()

            val text = "x = 0.0\ny = 0.0"
            view.sensorValues.text = text

            setupChart()
            sensorDataMap.forEach {
                val x = it.key.toFloat()
                val y = it.value[sensor - 1]

                addEntry(x, y)
            }

            view.viewTable.setOnClickListener {
                if (!view.sensorChart.isEmpty) {
                    val tableFragment = TableFragment()
                    val bundle = Bundle()
                    bundle.putSerializable("data_map", dataToMap(view.sensorChart.data.dataSets[0]) as Serializable)
                    tableFragment.arguments = bundle
                    tableFragment.show((context as AppCompatActivity).supportFragmentManager, tableFragment.tag)
                } else {
                    Toast.makeText(context, "No data available", Toast.LENGTH_SHORT).show()
                }
            }

            view.saveData.setOnClickListener {
                if (!view.sensorChart.isEmpty) {
                    DataHandler.storeData(context, sensor, view.sensorChart.data.dataSets[0])
                } else {
                    Toast.makeText(context, "No data available", Toast.LENGTH_SHORT).show()
                }
            }
        }

        private fun addEntry(x: Float, y: Float) {
            val data = view.sensorChart.data
            if (data != null) {
                var set = data.getDataSetByIndex(0)
                if (set == null) {
                    set = createSet()
                    data.addDataSet(set)
                }

                val text = "x = $x\ny = $y"
                view.sensorValues.text = text

                data.addEntry(Entry(x, y), 0)
                data.notifyDataChanged()

                view.sensorChart.notifyDataSetChanged()
                view.sensorChart.setVisibleXRangeMaximum(30F)
                view.sensorChart.moveViewToX(data.entryCount.toFloat())
            }
        }

        private fun createSet(): LineDataSet {
            val set = LineDataSet(null, "Dynamic Data")
            set.apply {
                axisDependency = YAxis.AxisDependency.LEFT
                color = ContextCompat.getColor(context, R.color.colorPrimary)
                setCircleColor(ContextCompat.getColor(context, R.color.colorAccent))
                lineWidth = 2F
                circleRadius = 4F
                setCircleColorHole(ContextCompat.getColor(context, R.color.colorAccent))
                fillAlpha = 0
                valueTextColor = Color.BLACK
                valueTextSize = 9f
                setDrawValues(false)
                mode = LineDataSet.Mode.LINEAR
                highlightLineWidth = 1.2F
            }

            return set
        }
    }
}