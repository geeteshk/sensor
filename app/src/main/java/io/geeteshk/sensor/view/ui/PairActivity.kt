package io.geeteshk.sensor.view.ui

import android.content.DialogInterface
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rx.ReplayingShare
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.RxBleDevice
import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import com.trello.rxlifecycle2.kotlin.bindUntilEvent
import io.geeteshk.sensor.App
import io.geeteshk.sensor.R
import io.geeteshk.sensor.extension.notifyObserver
import io.geeteshk.sensor.view.ItemOffsetDecoration
import io.geeteshk.sensor.view.adapter.DevicesAdapter
import io.geeteshk.sensor.view.adapter.GraphAdapter
import io.geeteshk.sensor.view.adapter.SensorsAdapter
import io.geeteshk.sensor.viewmodel.PairViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_pair.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class PairActivity : RxAppCompatActivity() {

    private lateinit var adapter: SensorsAdapter
    private var deviceAddress = ""

    private lateinit var bleDevice: RxBleDevice

    private lateinit var connectionObservable: Observable<RxBleConnection>

    private lateinit var saveItem: MenuItem

    private var isPaused = false

    private val compositeDisposable = CompositeDisposable()

    private lateinit var viewModel: PairViewModel

    private lateinit var graphsAdapter: GraphAdapter

    private var currentSensors = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pair)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this).get(PairViewModel::class.java)
        deviceAddress = intent.getStringExtra(DevicesAdapter.ADDRESS_EXTRA)
        bleDevice = App.bleClient.getBleDevice(deviceAddress)

        supportActionBar?.title = bleDevice.name ?: "Unknown"
        supportActionBar?.subtitle = bleDevice.macAddress ?: "00:00:00:00:00:00"
        adapter = SensorsAdapter(this)

        graphsAdapter = GraphAdapter(this)
        graphsView.layoutManager = LinearLayoutManager(this)
        graphsView.itemAnimator = DefaultItemAnimator()
        graphsView.adapter = graphsAdapter
        graphsView.addItemDecoration(ItemOffsetDecoration(this, R.dimen.item_offset))

        viewModel.sensorDataMap.observe(this, Observer {
            graphsAdapter.updateSensorData(it)
        })

        viewModel.visibleSensors.observe(this, Observer {
            graphsAdapter.updateVisibleSensors(it)
        })

        // Initialize the connection state
        connectionObservable = bleDevice.establishConnection(false)
                .bindUntilEvent(this, ActivityEvent.DESTROY)
                .compose(ReplayingShare.instance())

        // Try and establish a connection
        compositeDisposable.add(connectionObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Timber.d("Connection established successfully!")
                    writeData("#")
                    pauseData.setOnClickListener {
                        isPaused = if (isPaused) {
                            saveItem.isEnabled = false
                            pauseData.setImageResource(R.drawable.ic_pause)
                            false
                        } else {
                            // Write the exit code for the previous sensor to break the loop
                            writeData("#")

                            saveItem.isEnabled = true
                            pauseData.setImageResource(R.drawable.ic_refresh)
                            true
                        }
                    }

                    setupNotify()
                }, {
                    Timber.e(it)
                    finish()
                    Toast.makeText(this, "Connection closed by BLE device. Please try again.", Toast.LENGTH_SHORT).show()
                }, {
                    Timber.d("Connection finished.")
                }))
    }

    // Are we connected to a BLE device
    private fun isConnected() = bleDevice.connectionState == RxBleConnection.RxBleConnectionState.CONNECTED

    // Start listening for data changes in BLE
    private fun setupNotify() {
        if (isConnected()) {
            var reading = false
            var buffer = ""

            compositeDisposable.add(connectionObservable
                    .flatMap { it.setupNotification(READ_UUID) }
                    .flatMap { it }
                    .map { String(it) }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ data ->
                        var currData = data.replace("\\s".toRegex(), "")
                        if (currData.startsWith("#") && currData != "#") {
                            buffer = ""
                            reading = true
                            currData = currData.drop(1)
                        }

                        if (reading) {
                            buffer += currData
                        }

                        if (currData.endsWith("#")) {
                            buffer = buffer.dropLast(1)
                            reading = false

                            val list = buffer.split(",") as ArrayList
                            if (!isPaused) {
                                val time = list[0]
                                list.removeAt(0)

                                val parsedTime = time.split(":").map { it.toLong() }
                                val seconds = parsedTime[0] * 60 * 60 + parsedTime[1] * 60 + parsedTime[2]

                                viewModel.sensorDataMap.value!![seconds] = list.map { it.toFloat() }.toMutableList()
                                viewModel.sensorDataMap.notifyObserver()
                            }
                        }
                    }, {
                        Timber.e(it)
                    }))
        }
    }

    // Sending data to BLE
    private fun writeData(input: String) {
        if (isConnected()) {
            compositeDisposable.add(connectionObservable
                    .firstOrError()
                    .flatMap {
                        it.writeCharacteristic(WRITE_UUID, input.toByteArray())
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe ({ data ->
                        Timber.d("WRITE: %s", String(data))
                    }, { err ->
                        Timber.e(err)
                    }))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_pair, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        saveItem = menu!!.findItem(R.id.action_save)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item!!.itemId) {
            R.id.current_sensor -> {
                val input = EditText(this)
                input.inputType = InputType.TYPE_CLASS_TEXT
                input.hint = "1,2,10-15,78"
                input.setText(currentSensors)

                AlertDialog.Builder(this)
                        .setTitle("Sensors")
                        .setMessage("Enter the sensors you wish to view separated by commas, " +
                                "use a hyphen for ranges. e.g 1,2,10-15,78")
                        .setView(input)
                        .setPositiveButton("Select") { d: DialogInterface, _: Int ->
                            val sensors = input.text.toString()
                            val newSensors = ArrayList<Int>()
                            if (sensors.matches("^((\\d+\\-\\d+)(,|\$)|(\\d+)(,|\$))+\$".toRegex())) {
                                currentSensors = sensors

                                val items = sensors.split(",")
                                items.forEach { item ->
                                    if (item.contains("-")) {
                                        val range = item.split("-").map { it.toInt() }
                                        for (i in range[0]..range[1]) {
                                            newSensors.add(i)
                                        }
                                    } else {
                                        newSensors.add(item.toInt())
                                    }
                                }

                                newSensors.sort()
                                viewModel.visibleSensors.value = newSensors
                                d.cancel()
                            } else {
                                input.error = "Please only enter in the format outlined."
                            }
                        }
                        .setNegativeButton(getString(android.R.string.cancel)) { i: DialogInterface, _: Int ->
                            i.cancel()
                        }
                        .show()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        isPaused = true
        compositeDisposable.clear()
    }

    companion object {
        val READ_UUID: UUID = UUID.fromString("d973f2e1-b19e-11e2-9e96-0800200c9a66")
        val WRITE_UUID: UUID = UUID.fromString("d973f2e2-b19e-11e2-9e96-0800200c9a66")
    }
}
