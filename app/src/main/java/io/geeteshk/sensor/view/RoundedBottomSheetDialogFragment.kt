package io.geeteshk.sensor.view

import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.geeteshk.sensor.R

open class RoundedBottomSheetDialogFragment : BottomSheetDialogFragment() {

    override fun getTheme() = R.style.BottomSheet_DialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?) =
            BottomSheetDialog(requireContext(), theme)
}