package io.geeteshk.sensor.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*
import kotlin.collections.ArrayList

class PairViewModel : ViewModel() {

    val sensorDataMap = MutableLiveData<TreeMap<Long, List<Float>>>().apply {
        value = TreeMap()
    }

    val visibleSensors = MutableLiveData<ArrayList<Int>>().apply {
        value = intArrayOf(1).toCollection(ArrayList())
    }
}